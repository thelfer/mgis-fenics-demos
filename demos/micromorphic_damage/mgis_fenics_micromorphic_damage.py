#!/usr/bin/env python
# coding: utf-8

# # Micromorphic damage approach to brittle fracture
#
# $\newcommand{\bvarepsilon}{\boldsymbol{\varepsilon}}
# \newcommand{\dx}{\,\text{dx}}
# \newcommand{\dev}{\operatorname{dev}}
# \newcommand{\tr}{\operatorname{tr}}
# \DeclareMathOperator*{\argmin}{arg min}$
# 
# ## FEniCS implementation
# 

# In[1]:


from dolfin import *
from mshr import *
import mgis.fenics as mf
import numpy as np
import matplotlib.pyplot as plt
from IPython.display import clear_output

N = 100
domain = Rectangle(Point(0, 0), Point(1, 1)) - Circle(Point(0.5, 0.5), 0.2, 40)
mesh = generate_mesh(domain, N)

Vu = VectorFunctionSpace(mesh, "CG", 1)
u  = Function(Vu, name="Displacement")

def top(x, on_boundary):
    return near(x[1], 1) and on_boundary
def internal(x, on_boundary):
    return near((x[0]-0.5)**2+(x[1]-0.5)**2, 0.2**2, 0.05) and on_boundary
Uimp = Expression(("0", "t"), t=1, degree=0)
bcu = [DirichletBC(Vu, Constant((0, 0)), internal),
       DirichletBC(Vu, Uimp, top)]
v = Function(Vu)
bcu[1].apply(v.vector())

Vd_chi = FunctionSpace(mesh, "CG", 1)
d_chi = Function(Vd_chi, name="MicromorphicDamage")
bcd = DirichletBC(Vd_chi, Constant(0.), internal)


# We now define the displacement $u$-problem by loading the `PhaseFieldDisplacement` behaviour. Since the damage variable has been declared as an external state variable for this problem, we must register it with the damage field `d`. We also retrieve the `HistoryFunction` internal state variable `H` which will then be used as an external state variable for the damage problem. We finally recall that automatic registration of the strain is used here.

# In[2]:


material_u = mf.MFrontNonlinearMaterial("./src/libBehaviour.so",
                                        "MicromorphicDamageI",
                                        hypothesis="plane_strain")
problem_u = mf.MFrontNonlinearProblem(u, material_u, quadrature_degree=0, bcs=bcu)
problem_u.solver.parameters["report"] = False

# Creating the local damage field on the integration point of the mechanical problem
d_tr = problem_u.get_state_variable("DamageEstimate")
# damage at the end of the time step
d = d_tr.copy()
# registring d as an external state variable
problem_u.register_external_state_variable("Damage", d)

# Similarly, the damage problem is defined from the `PhaseFieldDamage` behaviour. The latter is a generalized behaviour which involves two gradients (as discussed in [the corresponding section](#mfront-behaviour-for-the-damage-problem)) which must be registered with their corresponding UFL representations. As mentioned before, for this behaviour `HistoryFunction` is an external state variable which is now registered as being `H`.

# In[3]:


Gc, l0 = 1., 0.02
material_d_chi = mf.MFrontNonlinearMaterial("./src/libBehaviour.so",
                                           "MicromorphicDamageII",
                                           hypothesis="plane_strain",
                                           material_properties={"A_chi": l0,
                                                                "H_chi": Gc})

problem_d_chi = mf.MFrontNonlinearProblem(d_chi, material_d_chi, quadrature_degree=0, bcs=bcd)
problem_d_chi.register_gradient("MicromorphicDamage", d_chi)
problem_d_chi.register_gradient("MicromorphicDamageGradient", grad(d_chi))
problem_d_chi.register_external_state_variable("Damage", d)
problem_d_chi.solver.parameters["report"] = False

# We now implement the load-stepping procedure and, for each load step, the alternate minimization algorithm between the $u$-problem and the $d$-problem. The alternate minimization iterations are stopped when $\|d-d_{old}\|_\infty \leq \texttt{tol}$ between two consecutive damage fields. 
# 
# After convergence of an alternate minimization step, we compute the vertical force acting on the top surface. For this purpose, we previously defined a function `v` with a vertical component equal to 1 on the top boundary and zero everywhere. The action of the residual form in this field is precisely the resulting vertical force of the imposed boundary conditions.  
# We also compute the total stored and dissipated energy. The former has been defined in `PhaseFieldDisplacement.mfront` and the second in `PhaseFieldDamage.mfront`.

# In[4]:


tol, Nitermax = 1e-3, 500
loading = np.concatenate((np.linspace(0, 70e-3, 6),
                          np.linspace(70e-3, 125e-3, 26)[1:]))
N = loading.shape[0]
results = np.zeros((N, 3))
for (i, t) in enumerate(loading[1:]):
    print("Time step:", i+1)
    Uimp.t = t
    # Start alternate minimization
    res = 1.
    j = 1
    while res > tol and j < Nitermax:
        # Solve displacement u-problem
        problem_u.solve(u.vector())
        # update damage
        d_previous_values = d.vector().get_local()
        d.assign(d_tr)
        # Solve the micromorphic damage problem
        problem_d_chi.solve(d_chi.vector())
        # damage increment
        dd = d.vector().get_local()-d_previous_values
        res = np.max(dd)
        
        print("   Iteration {}: {}".format(j, res))
        j += 1

    results[i+1, 0] = assemble(action(problem_u.residual, v))
    # results[i+1, 1] = problem_u.get_stored_energy()
    # results[i+1, 2] = problem_d_chi.get_dissipated_energy()
    
    clear_output(wait = True)
    # plt.figure()
    # p=plot(d, vmin=0, vmax=1)
    # plt.colorbar(p)
    # plt.savefig("./results/phase_field_{:04d}.png".format(i), dpi=400)
    


# Load-displacement and energy evolution curves show that there is a phase of brutal crack nucleation followed by a more stable crack propagation phase towards the plate boundaries. The solution vertical symmetry is lost in the last load steps when approaching the plate boundaries as already mentioned in  [@bourdin2000numerical].

# In[5]:



plt.figure()
plt.plot(loading, results[:, 0], "-o")
plt.xlabel("Imposed displacement")
plt.ylabel("Vertical force")
plt.show()

# plt.figure()
# plt.plot(loading, results[:, 1], label="elastic energy")
# plt.plot(loading, results[:, 2], label="fracture energy")
# plt.plot(loading, results[:, 1] + results[:, 2], label="total energy")
# plt.xlabel("Imposed displacement")
# plt.ylabel("Energies")
# plt.legend()
# plt.show()

