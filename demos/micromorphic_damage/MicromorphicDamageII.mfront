@DSL DefaultGenericBehaviour;
@Behaviour MicromorphicDamageII;
@Author Thomas Helfer, Jérémy Bleyer;
@Date 21 / 09 / 2021;

@Gradient real dχ;
dχ.setEntryName("MicromorphicDamage");
@ThermodynamicForce real a;
a.setEntryName("MicromorphicDamageDualForce");

@Gradient TVector ∇dχ;
∇dχ.setEntryName("MicromorphicDamageGradient");
@ThermodynamicForce TVector b;
b.setEntryName("MicromorphicDamageGradientDualForce");

@TangentOperatorBlocks{∂a∕∂Δdχ, ∂b∕∂Δ∇dχ};

@MaterialProperty real Aχ;
Aχ.setEntryName("A_chi");
@MaterialProperty real Hχ;
Hχ.setEntryName("H_chi");

@ExternalStateVariable real d;
d.setGlossaryName("Damage");

@Integrator {
  a = -Hχ * (d + Δd - dχ - Δdχ);
  b = Aχ * (∇dχ + Δ∇dχ);
}

@TangentOperator {
  ∂a∕∂Δdχ = Hχ;
  ∂b∕∂Δ∇dχ = Aχ * tmatrix<N, N, real>::Id();
}
